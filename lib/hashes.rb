# EASY

# Define a method that, given a sentence, returns a hash of each of the words as
# keys with their lengths as values. Assume the argument lacks punctuation.
def word_lengths(str)
  words = str.split(" ")
  hash = {}

  words.each do |word|
    hash[word] = word.length
  end

  hash
end

# Define a method that, given a hash with integers as values, returns the key
# with the largest value.
def greatest_key_by_val(hash)
  array = hash.sort_by { |k, v| v }
  array[-1].first
end

# Define a method that accepts two hashes as arguments: an older inventory and a
# newer one. The method should update keys in the older inventory with values
# from the newer one as well as add new key-value pairs to the older inventory.
# The method should return the older inventory as a result. march = {rubies: 10,
# emeralds: 14, diamonds: 2} april = {emeralds: 27, moonstones: 5}
# update_inventory(march, april) => {rubies: 10, emeralds: 27, diamonds: 2,
# moonstones: 5}
def update_inventory(older, newer)
  newer.each do |k, v|
    older[k] = v
  end

  older
end

# Define a method that, given a word, returns a hash with the letters in the
# word as keys and the frequencies of the letters as values.
def letter_counts(word)
  hash = Hash.new(0)

  word.each_char do |ch|
    hash[ch] += 1
  end

  hash
end

# MEDIUM

# Define a method that, given an array, returns that array without duplicates.
# Use a hash! Don't use the uniq method.
def my_uniq(arr)
  hash = Hash.new(0)
  res = []

  arr.each do |ele|
    hash[ele] += 1
  end

  hash.to_a.each do |array|
    res << array.first
  end

  res
end

# Define a method that, given an array of numbers, returns a hash with "even"
# and "odd" as keys and the frequency of each parity as values.
def evens_and_odds(numbers)
  hash = Hash.new(0)

  numbers.each do |num|
    if num % 2 == 0
      hash[:even] += 1
    else
      hash[:odd] += 1
    end
  end

  hash
end

# Define a method that, given a string, returns the most common vowel. If
# there's a tie, return the vowel that occurs earlier in the alphabet. Assume
# all letters are lower case.
def most_common_vowel(string)
  vowels = "aeiou"
  hash = Hash.new(0)

  string.each_char do |ch|
    hash[ch] += 1 if vowels.include?(ch)
  end

  sorted_by_vowel = hash.sort_by { |vowel, count| vowel }
  sorted_by_count = sorted_by_vowel.sort { |a, b| b.last <=> a.last }

  sorted_by_count.first.first
end

# HARD

# Define a method that, given a hash with keys as student names and values as
# their birthday months (numerically, e.g., 1 corresponds to January), returns
# every combination of students whose birthdays fall in the second half of the
# year (months 7-12). students_with_birthdays = { "Asher" => 6, "Bertie" => 11,
# "Dottie" => 8, "Warren" => 9 }
# fall_and_winter_birthdays(students_with_birthdays) => [ ["Bertie", "Dottie"],
# ["Bertie", "Warren"], ["Dottie", "Warren"] ]
def fall_and_winter_birthdays(students)
  birthday_students = []
  res = []

  students.each do |student, month|
    if month > 6
      birthday_students << student
    end
  end

  birthday_students.each_with_index do |student, i|
    birthday_students.each_with_index do |student2, i2|
      if i < i2
        res << [student, student2]
      end
    end
  end

  res
end

# Define a method that, given an array of specimens, returns the biodiversity
# index as defined by the following formula: number_of_species**2 *
# smallest_population_size / largest_population_size biodiversity_index(["cat",
# "cat", "cat"]) => 1 biodiversity_index(["cat", "leopard-spotted ferret",
# "dog"]) => 9
def biodiversity_index(specimens)
  hash = Hash.new(0)

  specimens.each do |specimen|
    hash[specimen] += 1
  end

  specimen_count_array = hash.sort_by { |specimen, number| number }

  specimen_count_array.count ** 2 * specimen_count_array.first.last / specimen_count_array.last.last
end

# Define a method that, given the string of a respectable business sign, returns
# a boolean indicating whether pranksters can make a given vandalized string
# using the available letters. Ignore capitalization and punctuation.
# can_tweak_sign("We're having a yellow ferret sale for a good cause over at the
# pet shop!", "Leopard ferrets forever yo") => true
def can_tweak_sign?(normal_sign, vandalized_sign)
  normal_sign_hash = Hash.new(0)
  vandalized_sign_hash = Hash.new(0)

  normal_sign.each_char do |ch|
    normal_sign_hash[ch.downcase] += 1
  end

  vandalized_sign.each_char do |ch|
    vandalized_sign_hash[ch.downcase] += 1
  end

  vandalized_sign_hash.each do |ch_vandal, count_vandal|
    normal_sign_hash.each do |ch_normal, count_normal|
      if ch_vandal == ch_normal && count_normal < count_vandal
        return false
      end
    end
  end

  true
end

def character_count(str)

end
